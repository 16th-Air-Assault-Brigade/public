# Contributing Guidelines
- Always use UTF-8 encoding for text files
- Always use Markdown for documents. This allows us effectively collaborate. How to do stuff in Markdown: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet, https://docs.gitlab.com/ce/user/markdown.html
- Always use `*.md` for Markdown files
- Use the `resources` folder next to markdown file to store needed resources like images.
- For images store also it's source format so it's easily editable (for example store both gimp's `*.xcf` for editing and also the `*.png` for the display)
- In commit message always specify what are your changes about. If you're to do something with an issue, use issue referencing: https://docs.gitlab.com/ee/user/project/issues/crosslinking_issues.html
- When your commit solves an issue, use autoclosing (`Fix` followed by a reference, for example `Fix #6`)
