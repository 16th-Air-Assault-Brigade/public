# Fleet Event Rules
## General
- Fleet events are arranged by an Officer (referred as Event Master)
- These rules don't apply to all meeting-type events as there special rules for meetings shall apply

## Fleet Event Announcing
- Event Master announces the events both in-game and on Discord
- The announcement must contain:
    - Event name
    - Event Master (On Discord defaults to message poster)
    - Event time. In-game use relevant field. For Discord use [this](https://www.timeanddate.com/worldclock/fixedform.html)  so players can get their local time quickly 
    - Some description with additional details
- Event Master can request Admirals to place some short announcement into fleet MOTD. However, its limited space shall be respected

## Behavior at Fleet Events
- The Event Master **must** attend to the Event
- The Event Master is the absolute master of the event so all attendees should respect that regardless of their rank
- Event Master can kick people from the Event under following terms (at least one must apply):
    - The kicked attendee doesn't meet the announced event requirements
    - For unappropriate behavior
    
## Fleet Event Rewards
- The Event Master can request up to 3 items for a single event from Fleet Vault for rewarding the attendees
- These items are withdrawn from fleet bank by Admiral rank or above. The usual rules for using the Vault shall apply
- Event Master can't give more than one item from Vault to a single person on one event
- All the unused Vault items are to be used for next events

## After the Event
- Event Master can promote any attendee to rank 3
- Event Master writes a report (ideally as a Gitlab issue) containing:
    - Attendee list
    - Given vault items as rewards (who received the reward)
    - Optionally what happened at the event
    
## Punishments
- The Event Master can be instantly demoted if an `Epic Fail` occurs
- The Event Master can be demoted if repeated `Fails` are occurring
- The `Epic Fail` is:
    - When the Event Master doesn't attend to the Event without a prior notice (Any other Officer can take over the Event under this condition)
    - Using the Vault items in any other means than described here without permission
- The `Fail` is:
    - Overall poor event organization
    - Not properly announcing the Event
