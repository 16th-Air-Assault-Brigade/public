# Ground combat tips (beta version)

- on Defera in the alpha quadrant you can get many useful items while killing the borg
- do not waste borg items for weapons by the researchers within the camp -> buy a fractal modulator instead or use weapons with physical damage as mentioned in https://gitlab.com/16th-Air-Assault-Brigade/public/blob/master/knowledge-base/bro-tips.md
- complete PVE missions to get reputation marks for reputation jobs to receive special weapons
- buy a kit from exchange which is at least very rare and Mk XI for improving overall kit performance and armor expertise (unless you find a useful kit but this happens not very often)
- use kit modules like transphase bombs and transporters
- prepare your combat scene with fighting drones and a shield generator
- use weapons which not only damage the enemies but also push them back or better knock them over
- use sniper weapons and press X while shoting the enemies before they can shot you
- shot down the strongest enemies first
- play in a team with at least 3 players so if one gets shot down another one is able to revive him/her and the next one can continue to shot the enemy (it is really annoying when you respawn and need to get back to the combat place)
