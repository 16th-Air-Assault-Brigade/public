# Fleet dashboard

## News and updates
- Use armada channel for all non-fleet-internal chatting
- Latest fleet meeting: https://gitlab.com/16th-Air-Assault-Brigade/public/blob/master/fleet-meeting-logs/2019-01-25.md

## Rules
- Misc rules: https://gitlab.com/16th-Air-Assault-Brigade/public/tree/master/rules
- Fleet Rank info: https://gitlab.com/16th-Air-Assault-Brigade/public/blob/master/fleet-ranks.md

## Current events
- Saturday Warzone
- Joint Armada Task Force event on Wednesday 28th
- Need to find the time spot for the next Fleet Meeting

## Links
- Knowledge base: https://gitlab.com/16th-Air-Assault-Brigade/public
- Fleet Discord: https://discord.gg/zPGZNH9
- Armada Discord: https://discord.gg/M7WqtPR

## Roles and responsibilites
### Fleet Admins
- Brea@Chuaos (Gitlab account @Chuaos)
- Cherry@WoLF_KABOOM (Gitlab account @WoLFKABOOM)

### Gitlab Admin
- Ziktofel@ziktofel (Gitlab account @Ziktofel)

### Fleet Discord Moderator
- Aleyna@andy#5498 (Gitlab account @Aleyna_Scott)
