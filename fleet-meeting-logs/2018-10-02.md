# Fleet meeting on 2018-10-02

## Attendees
### Fleet Members
- Cherry
- Brea
- Texadasa
- Keel-Haul
- Ziktofel
- Tyler
- Aleyna

### Guests
- Pork (from P-FLEET)

## Fleet rank name challenge
- Initiated by Cherry
- Only one valid submission by Aleyna
- Cherry will change rank names soon according to the submission

## Discord channels
- Aleyna deleted discord channels with no recent activity 

## New gitlab dashboard
- Ziktofel asked for feedback
- No negative comments on the dashboard

## Open fleet slots within armada
- Search for new beta is still running
- Task is assigned to officers within the `16th`
